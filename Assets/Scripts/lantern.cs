using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lantern : MonoBehaviour
{
    [SerializeField]
    Material unlitSignMat;
    [SerializeField]
    Material litSignMat;

    public lanternSwitch controlSwitch;

    private Renderer meshRenderer;

    public Light lanternLight;

    public bool isOn;

    // Start is called before the first frame update
    void Start()
    {
        lanternLight = GetComponentInChildren<Light>();
        meshRenderer = GetComponent<MeshRenderer>();
        isOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        CheckSwitch();
        CheckLight();
    }

    private void CheckSwitch()
    {
        isOn = controlSwitch.isOnLocal;
    }

    private void CheckLight()
    {
        if(isOn)
        {
            lanternLight.enabled = true;
            meshRenderer.material = litSignMat;
        }
        else
        {
            lanternLight.enabled = false;
            meshRenderer.material = unlitSignMat;
        }
    }
}
