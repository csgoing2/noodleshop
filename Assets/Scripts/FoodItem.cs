using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodItem : MonoBehaviour
{
    public enum cookingTypes
    {
        None,
        Boiler,
        Fryer,
        Stove
    }

    public enum foodTypes
    {
        Noodle,
        Broth,
        Meat,
        Topping
    }

    //Component references
    public GameObject foodModel;
    public SphereCollider foodCookingArea;
    public cookingTypes requiredCookingMethod;
    public foodTypes itemFoodType;

    public AudioSource audioSource;

    //public variables
    public string itemName;
    public string displayedItemName;
    public float cookTimeTotal;
    public float elapsedCookTime;
    public float burnCookTime;
    public bool needToBeCooked;
    public bool isCooking;
    public bool isCooked;
    public bool isEaten;

    public bool isRequestedForOrder;

    public float costToPurchase;
    public int satisfactionAmount;

    public ParticleSystem steamEmission;

    public MeshRenderer foodRenderer;
    public Material uncooked;
    public Material cooked;
    public Material overcooked;

    public void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        isCooking = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        isEaten = false;
        foodModel = this.gameObject;
        //foodRenderer = foodModel.GetComponent<MeshRenderer>();
        if(needToBeCooked)
            foodCookingArea = this.gameObject.GetComponent<SphereCollider>();

        if(requiredCookingMethod == cookingTypes.Fryer)
        {
            steamEmission = GetComponent<ParticleSystem>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(needToBeCooked)
        {
            foodRenderer.material = CookItemMaterial();
            if (isCooking)
            {
                Cook();
                CookItemMaterial();
            }
        }
        else
        {
            foodRenderer.material = cooked;
            displayedItemName = itemName;
        }
        
        if(isEaten)
        {
            foodRenderer.gameObject.SetActive(false);
            Debug.Log(this.gameObject + " has been eaten.");
        }

        CreateSteam();
        PlaySound();

        //Debug.Log(this.gameObject);
        //Debug.Log(isEaten);
        // Debug.Log(foodCookingArea);
    }

    private void CreateSteam()
    {
        if(requiredCookingMethod == cookingTypes.Fryer)
        {
            if (!isCooking)
            {
                steamEmission.Stop();
                Debug.Log("Steam should be stopped");
            }

            if (isCooking && !steamEmission.isPlaying)
            {
                steamEmission.Play();
                Debug.Log("Steam should be made");
            }
        }
    }

    private void Cook()
    {
        elapsedCookTime += 1 * Time.deltaTime;
        if(elapsedCookTime > cookTimeTotal)
        {
            isCooked = true;
        }
    }

    private void PlaySound()
    {
        if(requiredCookingMethod == cookingTypes.Fryer)
        {
            if (!isCooking)
            {
                audioSource.Stop();
            }

            if (isCooking && !audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }

    private Material CookItemMaterial()
    {
        if(elapsedCookTime >= burnCookTime)
        {
            //satisfactionAmount = satisfactionAmount - 1;
            displayedItemName = "Overcooked " + itemName;

            return overcooked;
        }
        else if (elapsedCookTime >= cookTimeTotal || isCooked == true)
        {
            //satisfactionAmount = satisfactionAmount + 1;
            displayedItemName = "Cooked " + itemName;

            return cooked;
        }
        else
        {
            //satisfactionAmount = satisfactionAmount - 2;
            displayedItemName = "Uncooked " + itemName;
            return uncooked;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        CookingTool collidedCookingTool = null;    
        if(other.gameObject.GetComponent<CookingTool>())
        {
            collidedCookingTool = other.gameObject.GetComponent<CookingTool>();

            if(collidedCookingTool.selectedCookingMethod.ToString() == requiredCookingMethod.ToString() && needToBeCooked == true)
            {
                isCooking = true;
                //elapsedCookTime += 1 * Time.deltaTime; <-- This is being handled in Cook() function
            }    
        }

        //Debug.Log(other.gameObject.name.ToString());
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.GetComponent<CookingTool>())
        {
            isCooking = false;
            //sManager.Stop();
            Debug.Log("No Longer Cooking!");
        }
    }
}
