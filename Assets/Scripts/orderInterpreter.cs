using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orderInterpreter : MonoBehaviour
{
    public int satisfaction;
    public int maxSatisfaction;

    public customerController customer;

    public bowlController bowlController;

    public GameObject orderPlacement;
    public GameObject order;

    public string noodleName;
    public string brothName;
    public string brothAmount;
    public string meatName;
    public string toppingName;

    public List<string> presentedItemNames;
    public List<string> requestedItemNames;

    public List<GameObject> presentedOrder;
    public List<GameObject> requestedOrder;

    public List<string> itemsToBeEaten;
    public List<GameObject> objectsToBeEaten;

    public bool isHungry;
    public bool checkOrder;
    public bool moveOrder;
    public bool readOrder;

    public float itemPlaceSpeed;

    void Start()
    {
        isHungry = customer.isHungry;
        order = null;
        moveOrder = true;
        checkOrder = true;
        readOrder = true;
    }

    void Update()
    {
        CheckIfCustomerIsHungry();
        if(checkOrder && order != null)
        {
            if(readOrder)
            {
                ReadInItems();
            }

            requestedOrder = customer.orderRequest;
            
            CheckPresentedOrder();

            EatOrder();
        }
    }

    private void CheckIfCustomerIsHungry()
    {
        if (isHungry)
            return;
        else
        {
            isHungry = customer.isHungry;
        }
    }

    private void ReadInItems()
    {
        bowlController = order.GetComponent<bowlController>();

        //check noodle type
        if (!presentedOrder.Contains(bowlController.noodleRef))
        {
            presentedOrder.Add(bowlController.noodleRef);
        }
        noodleName = bowlController.noodleRef.GetComponent<FoodItem>().displayedItemName;
        //check broth amount
        brothAmount = bowlController.bowlStates.ToString();
        brothName = bowlController.brothTypes.ToString();
        //check meat
        if (!presentedOrder.Contains(bowlController.meatRef))
        {
            presentedOrder.Add(bowlController.meatRef);
        }
        meatName = bowlController.meatRef.GetComponent<FoodItem>().displayedItemName;
        //check topping
        if (!presentedOrder.Contains(bowlController.toppingRef))
        {
            presentedOrder.Add(bowlController.toppingRef);
        }
        toppingName = bowlController.toppingRef.GetComponent<FoodItem>().displayedItemName;

        readOrder = false;
    }

    private void CheckPresentedOrder()
    {
        
        //generate string lists of display names
        presentedItemNames = new List<string>();
        requestedItemNames = new List<string>();

        foreach (var item in presentedOrder)
        {
            presentedItemNames.Add(item.GetComponent<FoodItem>().displayedItemName);
            if(!presentedItemNames.Contains(bowlController.brothTypes.ToString()))
            {
                presentedItemNames.Add(bowlController.brothTypes.ToString());
            }
        }

        foreach (var item in requestedOrder)
        {
            requestedItemNames.Add(item.GetComponent<FoodItem>().displayedItemName);
        }

        CalculateMaxSatisfaction();
        

        //compare order items to orderRequested
        if(checkOrder)
        {
            for (int i = 0; i < requestedItemNames.Count; i++)
            {
                if (requestedItemNames.Contains(presentedItemNames[i]))
                {
                    int requestedIndex = requestedItemNames.IndexOf(presentedItemNames[i]);
                    if (requestedItemNames[requestedIndex].ToString() == presentedItemNames[i].ToString())
                    {
                        //Debug.Log("We have a match " + requestedItemNames[requestedIndex] + " and " + presentedItemNames[i]);
                        itemsToBeEaten.Add(presentedItemNames[i]);
                        Debug.Log(presentedItemNames[i]);
                        satisfaction += 1;
                    }
                    else
                    {
                        satisfaction -= 1;
                    }

                }
                else
                {
                    satisfaction -= 1;
                    Debug.Log("Mismatch");
                }
            }
            customer.satisfaction = satisfaction;
            checkOrder = false;
        }

        //create list of gameObjects to be eaten

        foreach (var item in presentedItemNames)
        {
            for (int i = 0; i < presentedOrder.Count; i++)
            {
                if(presentedItemNames.Contains(presentedOrder[i].gameObject.GetComponent<FoodItem>().displayedItemName) && !objectsToBeEaten.Contains(presentedOrder[i]))
                {
                    objectsToBeEaten.Add(presentedOrder[i]);
                }
            }
        }
    }

    private void CalculateMaxSatisfaction()
    {
        //maxSatisfaction = requestedItemNames.Count; //generated here
        //customer.maxSatisfaction = maxSatisfaction;

        foreach (var item in requestedOrder)
        {
            maxSatisfaction += item.GetComponent<FoodItem>().satisfactionAmount;
        }
        maxSatisfaction += bowlController.satisfactionAmount;
        Debug.Log(maxSatisfaction);
        customer.maxSatisfaction = maxSatisfaction;
    }

    private void EatOrder()
    {
        isHungry = false;
        customer.isHungry = isHungry;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Bowl"))
        {
            order = other.gameObject;
            customer.isGivenOrder = true;
            customer.focus = order;
            customer.bController = other.GetComponent<bowlController>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Bowl"))
        {
            order = null;
            readOrder = true;
            customer.isGivenOrder = false;
            customer.bController = null;
        }
    }
}
