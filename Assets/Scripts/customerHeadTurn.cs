using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class customerHeadTurn : MonoBehaviour
{
    public customerController parentController;
    public Transform attention;
    // Start is called before the first frame update
    void Start()
    {
        parentController = GetComponentInParent<customerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attention == null)
        {

        }

        attention = parentController.focus.transform;
        transform.LookAt(attention);
    }
}
