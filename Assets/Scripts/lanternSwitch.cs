using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lanternSwitch : MonoBehaviour
{
    public bool isOnLocal;

    [SerializeField]
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        isOnLocal = false;
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfOn();
    }

    private void CheckIfOn()
    {
        if(isOnLocal)
        {
            anim.SetBool("isOn", true);
        }
        else
        {
            anim.SetBool("isOn", false);
        }
    }

    public void TurnOn()
    {
        isOnLocal = !isOnLocal;
    }
}
