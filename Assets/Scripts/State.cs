using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State 
{
        protected customerController customer;
        protected StateMachine stateMachine;

        protected State(customerController customer, StateMachine stateMachine)
        {
            this.customer = customer;
            this.stateMachine = stateMachine;
        }

        public virtual void Enter()
        {

        }

        public virtual void LogicUpdate()
        {

        }

        public virtual void PhysicsUpdate()
        {

        }

        public virtual void Exit()
        {

        }
}
