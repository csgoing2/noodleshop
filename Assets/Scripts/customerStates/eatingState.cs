using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatingState : State
{
    public bool isHungry;
    public bool hasEaten;
    public bool isThankful;
    public bool isComplaining;

    public bool hasOrder;
    public bool m_eatNextOrderItem;

    public int satisfaction;
    public int maxSatisfaction;

    public List<GameObject> m_objectsToBeEaten;

    public EatingState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("Time to Eat");
        foreach (var item in customer.GetComponentInChildren<orderInterpreter>().objectsToBeEaten)
        {
            Debug.Log(customer.GetComponentInChildren<orderInterpreter>().objectsToBeEaten);
        }
        
        //satisfaction = customer.GetComponentInChildren<orderInterpreter>().satisfaction;
        //maxSatisfaction = customer.GetComponentInChildren<orderInterpreter>().maxSatisfaction;
        m_objectsToBeEaten = new List<GameObject>(customer.GetComponentInChildren<orderInterpreter>().objectsToBeEaten);
        Debug.Log("Made it past copying list");
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        foreach (var item in m_objectsToBeEaten)
        {
            Debug.Log(item.gameObject + " , ");
        }
        satisfaction = customer.GetComponentInChildren<orderInterpreter>().satisfaction;

        if(satisfaction >= maxSatisfaction)
        {
            isThankful = true;
            customer.isSatisfied = true;
        }
        else if (satisfaction < maxSatisfaction)
        {
            isComplaining = true;
            customer.isSatisfied = true;
        }

        //Debug.Log(isThankful + " - isThankful, " + isComplaining + " - isComplaining");

        //move all below to eatingState
        if (!customer.isHungry && customer.isSatisfied && isComplaining)
        {
            stateMachine.ChangeState(customer.complainState);
            Debug.Log("complaining");
        }
        /*else if (customer.isHungry && customer.isGivenOrder)
        {
            stateMachine.ChangeState(customer.eatingState);
        }*/
        else if (!customer.isHungry && customer.isSatisfied && isThankful)
        {
            stateMachine.ChangeState(customer.thankingState);
        }

    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
