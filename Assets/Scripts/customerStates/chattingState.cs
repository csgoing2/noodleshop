using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChattingState : State
{
    public Rigidbody customerRB;
    public ChattingState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        customerRB = customer.GetComponent<Rigidbody>();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if(customer.reachableSeats.Count >= 1 && customer.emptySeats.Count >= 1)
        {
            customerRB.constraints = RigidbodyConstraints.None;
            customer.chosenSeat = customer.emptySeats[0];
            customer.seatingLocation = customer.chosenSeat.transform.GetChild(0).gameObject;
            stateMachine.ChangeState(customer.hungryState);
        }
        else
        {
            //customer.focus = customer.patrons[0];
            customerRB.constraints = RigidbodyConstraints.FreezePosition;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
