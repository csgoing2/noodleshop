using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplainState : State
{
    public bool m_isSpeaking;
    public bool needsToComplain;


    public ComplainState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        m_isSpeaking = true;
        needsToComplain = true;
        Debug.Log("Is Complaining");
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (needsToComplain)
        {
            //Debug.Log("Updating customer controller");
            customer.isSpeaking = m_isSpeaking;
            needsToComplain = !needsToComplain;
        }

        m_isSpeaking = customer.isSpeaking;

        if (!m_isSpeaking)
        {
            stateMachine.ChangeState(customer.leavingState);
        }
        else
        {
            return;
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
