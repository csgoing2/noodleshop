using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamingState : State
{
    public Transform startingLocation;
    public Transform endingLocation;

    public GameObject mainCamera;

    private float startTime;
    private float lengthToExit;

    public float moveSpeed;

    public RoamingState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        customer.focus = mainCamera;
        customer.customerSpeed = customer.originalCustomerSpeed;
        moveSpeed = customer.customerSpeed;

        startingLocation = customer.leftSpawnLocation;
        endingLocation = customer.rightSpawnLocation;
        startTime = Time.time;
        lengthToExit = Vector3.Distance(startingLocation.position, endingLocation.position);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        float distCovered = (Time.time - startTime) * moveSpeed;
        float stepOfTrip = distCovered / lengthToExit;
        customer.transform.position = Vector3.Lerp(startingLocation.position, endingLocation.position, stepOfTrip); //customer.allSeats[0].transform.position

        if (customer.isHungry && customer.reachableSeats.Count >= customer.allSeats.Count)
        {
            stateMachine.ChangeState(customer.chattingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        //customer.gameObject.transform.LookAt(endingLocation);

        
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
