using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;

public class HungryState : State
{
    public GameObject managerObject;
    public gameManager manager;

    public Transform startingLocation;
    public Transform endingLocation;

    public seatController chosenSeat;

    public bool requestRefreshedList;
    public bool hasOrder;

    public int satisfaction;
    public int maxSatisfaction;

    private float startTime;
    private float lengthToExit;

    public List<GameObject> availableNoodlesForOrder;
    public List<GameObject> availableBrothForOrder;
    public List<GameObject> availableMeatForOrder;
    public List<GameObject> availableToppingForOrder;

    public GameObject chosenNoodle;
    public GameObject chosenBroth;
    public GameObject chosenMeat;
    public GameObject chosenTopping;

    public List<GameObject> order;

    public float moveSpeed;
    public HungryState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("Changed to Hungry State");
        managerObject = GameObject.FindGameObjectWithTag("Game Manager");
        manager = managerObject.GetComponent<gameManager>();
        customer.customerSpeed = customer.customerSpeed * 0.01f;
        moveSpeed = customer.customerSpeed;
        startingLocation = customer.transform;
        /*endingLocation = customer.rightSpawnLocation;*/
        startTime = Time.time;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        
        

        if (!hasOrder)
        {
            GenerateOrder();
        }
       
        //check ifSeated
        if(Vector3.Distance(customer.transform.position, customer.seatingLocation.transform.position) < 5)
        {
            customer.isSeated = true;
            chosenSeat = customer.chosenSeat.GetComponent<seatController>();
            chosenSeat.isOccupied = true;
        }
        else
        {
            customer.isSeated = false;
        }

        if(hasOrder && customer.isSeated && customer.isGivenOrder)
        {
            stateMachine.ChangeState(customer.eatingState);
        }
        
    }

    private void GenerateOrder()
    {
        order = new List<GameObject>();
        //get fresh list of items in scene
        requestRefreshedList = true;
        manager.refreshFoodList = requestRefreshedList;
        requestRefreshedList = false;
        //create lists
        availableNoodlesForOrder = new List<GameObject>(manager.availableNoodlesInScene);
        availableBrothForOrder = new List<GameObject>(manager.availableBrothInScene);
        availableMeatForOrder = new List<GameObject>(manager.availableMeatInScene);
        availableToppingForOrder = new List<GameObject>(manager.availableToppingInScene);
        //pick random range in each list

        chosenNoodle = PickOrderItem(chosenNoodle, availableNoodlesForOrder, manager.availableNoodlesInScene, true);
        chosenBroth = PickOrderItem(chosenBroth, availableBrothForOrder, manager.availableBrothInScene, false);
        chosenMeat = PickOrderItem(chosenMeat, availableMeatForOrder, manager.availableMeatInScene, true);
        chosenTopping = PickOrderItem(chosenTopping, availableToppingForOrder, manager.availableToppingInScene, false);

        order.Add(chosenNoodle);
        order.Add(chosenBroth);
        order.Add(chosenMeat);
        order.Add(chosenTopping);

        customer.orderRequest = new List<GameObject>(order); //stores order in Customer
        customer.updateCustomerUI = true;   
        hasOrder = true;
    }

    private GameObject PickOrderItem(GameObject chosenItem, List<GameObject> availableItemsForOrder, List<GameObject> availableItemsInScene, bool shouldRemove)
    {
        int randomIndex = Random.Range(0, availableItemsForOrder.Count);
        
        chosenItem = availableItemsForOrder[randomIndex];
        //Debug.Log(chosenItem.name);

        if(shouldRemove)
        {
            availableItemsInScene[randomIndex].GetComponent<FoodItem>().isRequestedForOrder = true;
            availableItemsInScene.RemoveAt(randomIndex);
            availableItemsForOrder.RemoveAt(randomIndex);
        }
        
        return chosenItem;
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();

        endingLocation = customer.seatingLocation.transform;
        lengthToExit = Vector3.Distance(startingLocation.position, endingLocation.position);
        float distCovered = (Time.time - startTime) * moveSpeed;
        float stepOfTrip = distCovered / lengthToExit;
        customer.transform.position = Vector3.Lerp(startingLocation.position, endingLocation.position, moveSpeed / 10);
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
