using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeavingState : State
{
    public Transform startingLocation;
    public Transform endingLocation;

    public seatController chosenSeat;

    private float startTime;
    private float lengthToExit;

    public float moveSpeed;

    public LeavingState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        chosenSeat = customer.chosenSeat.GetComponent<seatController>();
        customer.focus = customer.rightSpawnLocation.gameObject;
        //customer.customerSpeed = customer.originalCustomerSpeed;
        moveSpeed = customer.customerSpeed * 0.01f;
        startingLocation = customer.transform;
        endingLocation = customer.rightSpawnLocation;
        startTime = Time.time;
        lengthToExit = Vector3.Distance(startingLocation.position, endingLocation.position);
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        chosenSeat.isOccupied = false;
        float distCovered = (Time.time - startTime) * moveSpeed;
        float stepOfTrip = distCovered / lengthToExit;
        customer.transform.position = Vector3.Lerp(startingLocation.position, endingLocation.position, moveSpeed);
        lengthToExit = Vector3.Distance(startingLocation.position, endingLocation.position);

        if (lengthToExit <= 2f)
        {
            customer.toDestroy = true;
        }

        //Debug.Log(lengthToExit);
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
