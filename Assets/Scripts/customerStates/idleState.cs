using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    public bool vacantSeat;
    public bool isChatting;
    public bool isHungry;

    public IdleState(customerController customer, StateMachine stateMachine) : base(customer, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("Customer is in Idle State");
        customer.customerSpeed = 0;
        customer.focus = customer.leftSpawnLocation.gameObject;
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if(customer.isHungry)
        {
            Debug.Log("Changing to Roaming State");
            stateMachine.ChangeState(customer.roamingState);
        }

        if (customer.isLeaving)
        {
            Debug.Log("Changing to Leaving State");
            stateMachine.ChangeState(customer.leavingState);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override string ToString()
    {
        return base.ToString();
    }

}
