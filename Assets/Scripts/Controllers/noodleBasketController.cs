using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noodleBasketController : MonoBehaviour
{
    public GameObject noodleTarget;
    
    public GameObject noodle;
    public GameObject noodleChild;

    public Transform parentTransform;

    private Rigidbody noodleRB;

    public SphereCollider snapLocation;
    private bool dropItem;
    public bool grabbedObject;

    void Start()
    {
        
    }

    void Update()
    {
        //Debug.Log(dropItem);
        /*if(noodle != null)
        {
            parentTransform = this.GetComponentInParent<Transform>();

            Debug.Log(Vector3.Dot(parentTransform.up, Vector3.down));
            if(Vector3.Dot(parentTransform.up, Vector3.down) > 0)
            {
                //dropItem = true;
                
            }
        }*/

        GrabNoodles();
    }

    public void GrabNoodles()
    {
        if(grabbedObject)
        {
            noodle = noodleTarget;
            if (noodle != null)
            {
                //deactivate components
                noodleChild = noodle.gameObject.transform.GetChild(0).gameObject;
                noodleChild.SetActive(false);
                noodleRB = noodle.GetComponent<Rigidbody>();
                noodleRB.useGravity = false;
                noodleRB.constraints = RigidbodyConstraints.FreezeAll;

                //move object into place
                noodle.transform.position = Vector3.Lerp(noodle.transform.position, snapLocation.transform.position, 1f);
                if (Vector3.Distance(noodle.transform.position, snapLocation.transform.position) < .1f)
                {
                    noodle.transform.position = snapLocation.transform.position;
                    noodle.transform.SetParent(this.gameObject.transform);
                }
            }

            dropItem = false;
        }
        else if (!grabbedObject)
        {
            if(noodle != null && noodleRB != null)
            {
                noodleRB.constraints = RigidbodyConstraints.None;
                noodleRB.useGravity = true;
                noodleChild.SetActive(true);
                noodle.transform.SetParent(null);
                dropItem = true;
                noodleRB = null;
                noodle = null;
                noodleChild = null;
                noodleTarget = null;
                //Debug.Log("Unparent item");
            }

        }
    }

    public void SetGrabObject()
    {
        grabbedObject = true;
    }

    public void DropGrabObject()
    {
        grabbedObject = false;
    }

    public void DropNoodles()
    {
        /*Debug.Log("Drop Noodle");
        noodleRB.constraints = RigidbodyConstraints.None;
        noodleRB.useGravity = true;
        noodleChild.SetActive(true);
        noodle.transform.SetParent(null);
        dropItem = true;
        noodle = null;*/
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject);
        if(other.GetComponent<FoodItem>())
        {
            if (other.GetComponent<FoodItem>().itemFoodType.ToString() == "Noodle" && noodle == null && !dropItem)
            {
                noodleTarget = other.gameObject;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.GetComponent<FoodItem>())
        {
            if (other.GetComponent<FoodItem>().itemFoodType.ToString() == "Noodle" && noodle != null)
            {
                if(noodleChild.activeInHierarchy == false)
                    noodleChild.SetActive(true);

                /*noodleChild = null;
                noodleTarget = null;
                noodle = null;*/
            }
        }
    }
}
