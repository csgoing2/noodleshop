using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ladleController : MonoBehaviour
{
    public enum LadleState
    {
        Full,
        Empty
    }

    public enum BrothType
    {
        ChickenBroth,
        BeefBroth
    }

    public LadleState ladleState;
    public BrothType brothType;

    public GameObject broth;
    public GameObject emptyAmount;
    public GameObject fullAmount;

    public bowlController fillTarget;

    public Transform currentBrothHeight;
    public Vector3 currentBrothScale;

    public MeshRenderer brothRenderer;

    public Material brothColor;

    public float fillSpeed;

    public bool isEmpty;
    void Start()
    {
        fillSpeed = 0.2f;
        isEmpty = true;
    }

    void Update()
    {
        currentBrothHeight = broth.transform;
        currentBrothScale = broth.transform.localScale;

        CheckBrothHeight();
        CheckLadleRotation();
    }

    private void CheckLadleRotation()
    {
        if(Vector3.Angle(transform.up, Vector3.up) > 45.0f)
        {
            Debug.Log("IsEmpty should be true");
            if(fillTarget && isEmpty == false)
            {
                fillTarget.bowlStates += 1;
            }

            isEmpty = true;
        }
    }

    private void CheckBrothHeight()
    {
        if(isEmpty)
        {
            broth.transform.position = Vector3.Lerp(currentBrothHeight.position, emptyAmount.transform.position, fillSpeed);
            broth.transform.localScale = Vector3.Lerp(currentBrothScale, emptyAmount.transform.localScale, fillSpeed);
        }
        else if (!isEmpty)
        {
            broth.GetComponent<MeshRenderer>().material = brothColor;
            broth.transform.position = Vector3.Lerp(currentBrothHeight.position, fullAmount.transform.position, fillSpeed);
            broth.transform.localScale = Vector3.Lerp(currentBrothScale, fullAmount.transform.localScale, fillSpeed);
        }

        if (Vector3.Distance(currentBrothHeight.position, emptyAmount.transform.position) < .02f)
        {
            brothRenderer.enabled = false;
        }
        else
        {
            brothRenderer.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<FoodItem>())
        {
            if (other.GetComponent<FoodItem>().itemFoodType.ToString() == "Broth")
            {
                brothColor = other.GetComponent<FoodItem>().cooked;
                if (isEmpty == true)
                {
                    isEmpty = false;
                }
            }
        }
        

        if(other.GetComponent<bowlController>())
        {
            fillTarget = other.GetComponent<bowlController>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.GetComponent<bowlController>())
        {
            fillTarget = null;
        }
    }
}
