using System.Collections.Generic;
using UnityEngine;

public class bowlController : MonoBehaviour
{
    public bool isUsed;
    public enum BowlState
    {
        EMPTY,
        ONETHIRDFULL,
        TWOTHIRDFULL,
        FULL
    }

    public enum BrothType
    {
        ChickenBroth,
        BeefBroth
    }

    public BowlState bowlStates;
    public BrothType brothTypes;
    public GameObject broth;

    public GameObject empty;
    public GameObject thirdFullAmount;
    public GameObject sixthFullAmount;
    public GameObject fullAmount;

    public GameObject noodlePlacement;
    public GameObject meatPlacement;
    public GameObject toppingPlacement;

    public List<GameObject> bowlIngredients;

    public GameObject noodleRef;
    public GameObject meatRef;
    public GameObject toppingRef;
    public GameObject parentTarget;

    public Transform currentBrothHeight;
    public Vector3 currentBrothScale;

    public Transform spillCheck;

    public int satisfactionAmount;

    public bool isEmpty;
    public bool oneThirdFull;
    public bool twoThirdsFull;
    public bool full;
  
    public float fillSpeed;
    public float itemPlaceSpeed;

    public float startTime;

    public float hitDistance;
    public GameObject otherFood;
    public FoodItem otherFoodType;

    private Collider[] colliders;
    private Collider[] colChildren;

    // Start is called before the first frame update
    void Start()
    {
        fillSpeed = 0.01f;
        itemPlaceSpeed = 0.2f;
        isEmpty = true;
        startTime = Time.time;
        //bowlStates = BowlState.EMPTY;
    }

    // Update is called once per frame
    void Update()
    {
        currentBrothHeight = broth.transform;
        currentBrothScale = broth.transform.localScale;

        CheckBrothHeight();


    }

    private void FixedUpdate()
    {
        /*MoveItemIntoPlace(noodlePlacement, noodleRef);
        MoveItemIntoPlace(meatPlacement, meatRef);
        MoveItemIntoPlace(toppingPlacement, toppingRef);*/

        //CheckBowlRotation();

        MoveIngredients();
    }

    private void MoveIngredients()
    {
        foreach (var item in bowlIngredients)
        {
            otherFood = item.gameObject;
            otherFoodType = item.GetComponent<FoodItem>();

            otherFoodType.itemFoodType = item.gameObject.GetComponent<FoodItem>().itemFoodType;

            switch (otherFoodType.itemFoodType)
            {
                case FoodItem.foodTypes.Noodle:
                    noodleRef = otherFood;
                    //noodleRef.GetComponent<SphereCollider>().enabled = false; //use <collider> instead of specific colliders
                    //noodleRef.GetComponent<BoxCollider>().enabled = false;
                    DisableColliders(noodleRef);
                    MoveItemIntoPlace(noodlePlacement, noodleRef);

                    break;
                case FoodItem.foodTypes.Meat:
                    meatRef = otherFood;
                    //meatRef.GetComponent<MeshCollider>().enabled = false;
                    DisableColliders(meatRef);
                    MoveItemIntoPlace(meatPlacement, meatRef);

                    break;
                case FoodItem.foodTypes.Topping:
                    toppingRef = otherFood;
                    //toppingRef.GetComponent<SphereCollider>().enabled = false;
                    //toppingRef.GetComponent<BoxCollider>().enabled = false;
                    DisableColliders(toppingRef);
                    MoveItemIntoPlace(toppingPlacement, toppingRef);
                    break;
                default:
                    break;
            }
        }
    }

    private void CheckBowlRotation()
    {
        
        //Debug.Log(Vector3.Angle(spillCheck.up, Vector3.up));

        RaycastHit hit;

        if (Physics.Raycast(spillCheck.gameObject.transform.position, spillCheck.TransformDirection(Vector3.forward), out hit, hitDistance))
        {
            if(!hit.transform.gameObject.GetComponent<FoodItem>())
            {
                Debug.DrawRay(spillCheck.gameObject.transform.position, spillCheck.TransformDirection(Vector3.forward) * hitDistance, Color.yellow);
                Debug.Log("Upside Down");
                if (Vector3.Angle(spillCheck.up, Vector3.up) < 95.0f || Vector3.Angle(spillCheck.up, Vector3.up) > 85)
                {
                    Debug.Log("Dropping bowl");

                    isEmpty = true;
                }
            }
            
        }
        else
        {
            Debug.DrawRay(spillCheck.gameObject.transform.position, spillCheck.TransformDirection(Vector3.forward) * hitDistance, Color.white);
        }

        if (Vector3.Angle(spillCheck.up, Vector3.up) > 95.0f || Vector3.Angle(spillCheck.up, Vector3.up) < 85)
        {
            Debug.Log("tilt");

            isEmpty = true;
        }
    }



    private void CheckBrothHeight()
    {

        switch (bowlStates)
        {
            case BowlState.EMPTY:
                broth.transform.position = Vector3.Lerp(currentBrothHeight.position, empty.transform.position, fillSpeed);
                broth.transform.localScale = Vector3.Lerp(currentBrothScale, empty.transform.localScale, fillSpeed);
                full = false;
                twoThirdsFull = false;
                oneThirdFull = false;
                satisfactionAmount = 0;
                break;
            case BowlState.ONETHIRDFULL:
                broth.transform.position = Vector3.Lerp(currentBrothHeight.position, thirdFullAmount.transform.position, fillSpeed);
                broth.transform.localScale = Vector3.Lerp(currentBrothScale, thirdFullAmount.transform.localScale, fillSpeed);
                full = false;
                twoThirdsFull = false;
                isEmpty = false;
                satisfactionAmount = 1;
                break;
            case BowlState.TWOTHIRDFULL:
                broth.transform.position = Vector3.Lerp(currentBrothHeight.position, sixthFullAmount.transform.position, fillSpeed);
                broth.transform.localScale = Vector3.Lerp(currentBrothScale, sixthFullAmount.transform.localScale, fillSpeed);
                full = false;
                oneThirdFull = false;
                isEmpty = false;
                satisfactionAmount = 2;
                break;
            case BowlState.FULL:
                broth.transform.position = Vector3.Lerp(currentBrothHeight.position, fullAmount.transform.position, fillSpeed);
                broth.transform.localScale = Vector3.Lerp(currentBrothScale, fullAmount.transform.localScale, fillSpeed);
                twoThirdsFull = false;
                oneThirdFull = false;
                isEmpty = false;
                satisfactionAmount = 3;
                break;
            default:
                break;
        }
    }



    private void DisableColliders(GameObject target)
    {
        /*        if (grabbedObject.GetComponent<MeshCollider>())
                    grabbedObject.GetComponent<MeshCollider>().enabled = false;

                if (grabbedObject.GetComponent<SphereCollider>())
                    grabbedObject.GetComponent<SphereCollider>().enabled = false;

                if (grabbedObject.GetComponent<BoxCollider>())
                    grabbedObject.GetComponent<BoxCollider>().enabled = false;*/

        colliders = target.GetComponents<Collider>();
        foreach (var item in colliders)
        {
            item.enabled = false;
        }

        colChildren = target.GetComponentsInChildren<Collider>();
        foreach (var item in colChildren)
        {
            item.enabled = false;

            if (item.isTrigger)
            {
                item.enabled = true;
            }
        }
    }

    private void EnableColliders(GameObject target)
    {
        /*        if (grabbedObject.GetComponent<MeshCollider>())
                    grabbedObject.GetComponent<MeshCollider>().enabled = true;

                if (grabbedObject.GetComponent<SphereCollider>())
                    grabbedObject.GetComponent<SphereCollider>().enabled = true;

                if (grabbedObject.GetComponent<BoxCollider>())
                    grabbedObject.GetComponent<BoxCollider>().enabled = true;*/

        colliders = target.GetComponents<Collider>();
        foreach (var item in colliders)
        {
            item.enabled = true;
        }

        colChildren = target.GetComponentsInChildren<Collider>();
        foreach (var item in colChildren)
        {
            item.enabled = true;
        }

        
    }

    private void MoveItemIntoPlace(GameObject itemPlacement, GameObject itemRef)
    {

        /*if (itemRef == null)
            return;*/

        float lengthToPlace = Vector3.Distance(itemRef.transform.position, itemPlacement.transform.position);

        float distCovered = (Time.time - startTime) * itemPlaceSpeed;
        float stepOfTrip = distCovered / lengthToPlace;
        Debug.Log(itemRef);
        if (lengthToPlace > 0.06f)
        {
            //itemRef.transform.localScale = Vector3.Lerp(itemRef.transform.localScale, itemPlacement.transform.localScale, itemPlaceSpeed);
            itemRef.transform.position = Vector3.Lerp(itemRef.transform.position, itemPlacement.transform.position, itemPlaceSpeed);
            //itemRef.transform.position = Vector3.Lerp(itemRef.transform.position, itemPlacement.transform.position, stepOfTrip);
        }

        if (lengthToPlace <= .06f)
        {
            itemRef.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            itemRef.transform.SetParent(itemPlacement.transform);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Food Item"))
        {
            //otherFoodType = new FoodItem();
            if(!bowlIngredients.Contains(other.gameObject))
            bowlIngredients.Add(other.gameObject);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Food Item"))
        {
            //otherFoodType = new FoodItem();

            otherFood = other.gameObject;
            otherFoodType = other.GetComponent<FoodItem>();

            otherFoodType.itemFoodType = other.gameObject.GetComponent<FoodItem>().itemFoodType;

            switch (otherFoodType.itemFoodType)
            {
                case FoodItem.foodTypes.Noodle:
                    DisableColliders(noodleRef);
                    noodleRef.transform.parent = null;
                    noodleRef = null;
                    break;
                case FoodItem.foodTypes.Meat:
                    DisableColliders(meatRef);
                    meatRef.transform.parent = null;
                    meatRef = null;
                    break;
                case FoodItem.foodTypes.Topping:
                    DisableColliders(toppingRef);
                    toppingRef.transform.parent = null;
                    toppingRef = null;
                    break;
                default:
                    break;
            }

            other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            other.GetComponent<Rigidbody>().useGravity = true;
            Debug.Log("Remove item from bowl");
        }
    }    
}
