using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class tongController : MonoBehaviour
{
    private Animator animator;

    public float grabDistance;

    public Collider grabPosition;
    public TextMeshPro grabNameDisplay;

    public FoodItem nameTextCache;

    public GameObject grabbableObject;
    public GameObject grabbedObject;
    private Rigidbody grabbedRB;

    private Collider[] colChildren;
    private Collider[] colliders;

    private void Start()
    {
        animator = GetComponent<Animator>();
        grabNameDisplay = GetComponentInChildren<TextMeshPro>();
    }

    private void Update()
    {
        UpdateTongText();


/*        foreach (var item in grabbableObjects)
        {
            Debug.Log(item + " , ");
        }*/
    }

    private void FixedUpdate()
    {
        Raycast();
    }

    private void Raycast()
    {
        RaycastHit hit;


        if (Physics.Raycast(grabPosition.transform.position, grabPosition.transform.TransformDirection(Vector3.forward), out hit, grabDistance))
        {
            if(hit.transform.gameObject.CompareTag("Food Item"))
            {
                Debug.DrawRay(grabPosition.transform.position, Vector3.forward, Color.green);
                grabbableObject = hit.transform.gameObject;
            }
        }
        else
        {
            Debug.DrawRay(grabPosition.transform.position, Vector3.forward, Color.red);
            grabbableObject = null;
        }

        //Debug.Log(grabbableObject);
    }

    public void CloseTongs()
    {
        if (animator.GetBool("isClosed") != true)
        {
            animator.SetBool("isClosed", true);
        }

        if (grabbableObject)
        {
            //grabbedObject = grabbableObjects[0];
            grabbedObject = grabbableObject;
            DisableColliders(grabbedObject);
            grabbedRB = grabbedObject.GetComponent<Rigidbody>();
            grabbedRB.useGravity = false;
            grabbedRB.constraints = RigidbodyConstraints.FreezePosition;
            grabbedRB.constraints = RigidbodyConstraints.FreezeRotation;
            grabbedObject.transform.position = grabPosition.transform.position;
            grabbedObject.transform.SetParent(grabPosition.gameObject.transform);
            Debug.Log("Grabbing");
        }

    }

    private GameObject GetClosestGrabObject(List<GameObject> grabbableObjects)
    {
        GameObject closestObject = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (var item in grabbableObjects)
        {
            float dist = Vector3.Distance(item.transform.position, currentPos);
            if(dist < minDist)
            {
                closestObject = item;
                minDist = dist;
            }
        }
        return closestObject;
    }

    public void OpenTongs()
    {
        animator.SetBool("isClosed", false);
        if(grabbedRB != null)
        {
            grabbedRB.constraints = RigidbodyConstraints.None;
            grabbedRB.useGravity = true;
            grabbedRB = null;
        }
        
        if(grabbedObject != null)
        {
            EnableColliders(grabbedObject);
            grabbedObject.transform.SetParent(null);
            grabbedObject = null;
        }
    }

    private void UpdateTongText()
    {
        if (grabbableObject)
        {
            if(grabbableObject.GetComponent<FoodItem>())
            {
                nameTextCache = grabbableObject.GetComponent<FoodItem>();
                grabNameDisplay.SetText(nameTextCache.displayedItemName);
            }
        }
        else
        {
            grabNameDisplay.SetText("");
        }
    }

    private void EnableColliders(GameObject target)
    {
/*        if (grabbedObject.GetComponent<MeshCollider>())
            grabbedObject.GetComponent<MeshCollider>().enabled = true;

        if (grabbedObject.GetComponent<SphereCollider>())
            grabbedObject.GetComponent<SphereCollider>().enabled = true;

        if (grabbedObject.GetComponent<BoxCollider>())
            grabbedObject.GetComponent<BoxCollider>().enabled = true;*/

/*        colliders = target.GetComponents<Collider>();
        foreach (var item in colliders)
        {
            item.enabled = true;
        }*/

        colChildren = target.GetComponentsInChildren<Collider> ();
        foreach (var item in colChildren)
        {
            item.enabled = true;
        }
    }

    private void DisableColliders(GameObject target)
    {
/*        if (grabbedObject.GetComponent<MeshCollider>())
            grabbedObject.GetComponent<MeshCollider>().enabled = false;

        if (grabbedObject.GetComponent<SphereCollider>())
            grabbedObject.GetComponent<SphereCollider>().enabled = false;

        if (grabbedObject.GetComponent<BoxCollider>())
            grabbedObject.GetComponent<BoxCollider>().enabled = false;*/

/*        colliders = target.GetComponents<Collider>();
        foreach (var item in colliders)
        {
            item.enabled = false;
        }*/

        colChildren = target.GetComponentsInChildren<Collider>();
        foreach (var item in colChildren)
        {
            item.enabled = false;

            if (item.isTrigger)
            {
                item.enabled = true;
            }
        }
    }

}

