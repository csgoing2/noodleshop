using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class customerController : MonoBehaviour
{
    public gameManager manager;

    public orderInterpreter orderInterpreter;

    public bowlController bController;

    public Transform leftSpawnLocation;
    public Transform rightSpawnLocation;
    public Transform attention;

    public int satisfaction;
    public int maxSatisfaction;

    public float timeWillingToWait;
    public float timeWaited;
    public float originalCustomerSpeed;
    public float customerSpeed;

    public bool isHungry;
    public bool isSatisfied;
    public bool isSpeaking;
    public bool isSeated;
    public bool isGivenOrder;
    public bool eatNextOrderItem;
    public bool isLeaving;
    public bool toDestroy;

    public bool updateCustomerUI;

    public GameObject chosenSeat;
    public GameObject seatingLocation;
    public GameObject focus;

    public GameObject orderCanvas;
    public GameObject orderPrefab;
    public GameObject orderText;

    public GameObject[] seatstoList;
    public List<GameObject> allSeats;
    public List<GameObject> reachableSeats;
    public List<GameObject> emptySeats;

    public List<GameObject> patrons;
    public List<GameObject> unSeatedPatrons;

    public List<GameObject> orderRequest;
    public List<GameObject> providedOrder;

    public StateMachine customerSM;
    public IdleState idleState;
    public ChattingState chattingState;
    public RoamingState roamingState;
    public HungryState hungryState;
    public EatingState eatingState;
    public ComplainState complainState;
    public ThankingState thankingState;
    public LeavingState leavingState;

    private void Awake()
    {
        leftSpawnLocation = GameObject.Find("AISpawnLeft").transform;
        rightSpawnLocation = GameObject.Find("AISpawnRight").transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        isHungry = true;
        //isSpeaking = false;
        originalCustomerSpeed = customerSpeed;
        customerSM = new StateMachine();
        seatstoList = GameObject.FindGameObjectsWithTag("Seat");
        eatNextOrderItem = false;

        foreach (var item in seatstoList)
        {
            allSeats.Add(item.gameObject);
        }

        idleState = new IdleState(this, customerSM);
        chattingState = new ChattingState(this, customerSM);
        roamingState = new RoamingState(this, customerSM);
        hungryState = new HungryState(this, customerSM);
        eatingState = new EatingState(this, customerSM);
        complainState = new ComplainState(this, customerSM);
        thankingState = new ThankingState(this, customerSM);
        leavingState = new LeavingState(this, customerSM);

        customerSM.Initialize(idleState);
    }

    // Update is called once per frame
    void Update()
    {
        CheckFocus();
        CheckOccupiedSeats();
        CheckSeatedPatrons();
        WaitingTime();
        customerSM.CurrentState.LogicUpdate();
        UpdateUI();

/*        if(satisfaction >= maxSatisfaction)
        {
            isSatisfied = true;
            isHungry = false;
        }*/

        providedOrder = new List<GameObject>(orderInterpreter.presentedOrder);
        
        if(providedOrder.Count >= 0)
        {
                for (int i = 0; i < providedOrder.Count; i++)
                {
                    GameObject item = providedOrder[i];
                    if (item.GetComponent<FoodItem>())
                    {
                        item.GetComponent<FoodItem>().isEaten = true;
                    }
                    //Destroy(item); // start ienumerator
                }

                if (bController != null)
                {
                    bController.bowlStates = bowlController.BowlState.EMPTY;
                    bController.isUsed = true;
                    Debug.Log("EmptyBowl");
                }
        }

        //Debug.Log(isSpeaking);
        if(isSatisfied)
        {
            //isSpeaking = true;
            if (isSpeaking)
            {
                Debug.Log("Should start coroutine");
                StartCoroutine(CustomerWait());
            }
        }
     

        if (toDestroy)
        {
            Debug.Log("Destroy this");
            manager.customers.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
        Debug.Log(customerSM.CurrentState.ToString());
    }

    private void UpdateUI()
    {
        if(!updateCustomerUI)
        {
            return;
        }
        else
        {
            for (int i = 0; i < orderRequest.Count; i++)
            {
                orderText = (GameObject)Instantiate(orderPrefab, orderCanvas.transform);

                //Debug.Log(orderText.gameObject);
                orderText.GetComponent<TextMeshProUGUI>().SetText(orderRequest[i].gameObject.GetComponent<FoodItem>().itemName.ToString());
            }
            updateCustomerUI = false;
        }
    }

    private void CheckFocus()
    {
        if(focus == null)
        {
            return;
        }
        else
        {
            attention = focus.transform;
        }
    }

    private void CheckSeatedPatrons()
    {
        for (int i = 0; i < patrons.Count; i++)
        {
            GameObject patron = patrons[i];

            if(patron != null && !patron.GetComponent<customerController>().isSeated && !unSeatedPatrons.Contains(patron) && !patron.GetComponent<customerController>().isLeaving)
            {
                unSeatedPatrons.Add(patron);
            }
        }
    }

    private void WaitingTime()
    {
        if(!isLeaving && !isSeated)
        {
            timeWaited += Time.deltaTime;
        }

        if(timeWaited > timeWillingToWait)
        {
            isLeaving = true;
        }
    }

    private void CheckOccupiedSeats()
    {
        for (int i = 0; i < reachableSeats.Count; i++)
        {
            GameObject item = reachableSeats[i];
            if (item.GetComponent<seatController>().isOccupied == false && !emptySeats.Contains(item))
            {
                emptySeats.Add(item);
            }

            if (item.GetComponent<seatController>().isOccupied == true)
            {
                emptySeats.Remove(item);
            }
        }
    }

    private void FixedUpdate()
    {
        customerSM.CurrentState.PhysicsUpdate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Seat"))
        {
            reachableSeats.Add(other.gameObject);
        }

        if(other.gameObject.CompareTag("Customer"))
        {
            if(!patrons.Contains(other.gameObject))
            {
                patrons.Add(other.gameObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Seat"))
        {
            reachableSeats.Remove(other.gameObject);
            emptySeats.Remove(other.gameObject);
        }

        if(other.gameObject.CompareTag("Customer"))
        {
            patrons.Remove(other.gameObject);
            unSeatedPatrons.Remove(other.gameObject);
        }
    }

    IEnumerator CustomerWait()
    {
        Debug.Log("Starting coroutine");
        yield return new WaitForSeconds(5);
        isSpeaking = false;
    }

    IEnumerator CustomerEat()
    {
        yield return new WaitForSeconds(2);
        //eatNextOrderItem = true;
        isSatisfied = true;
    }
}
