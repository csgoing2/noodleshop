using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    [SerializeField]
    float timeUntilNextSpawn;
    [SerializeField]
    float elapsedTime;

    [SerializeField]
    bool isOpen;

    public lantern shopLantern;

    public Transform leftSpawnLocation;
    public Transform rightSpawnLocation;

    public float numberOfCustomers;
    public float maxNumberOfCustomers;
    public float numberOfHungryCustomers;
    public float numberOfLeavingCustomers;

    public bool toSpawnCustomer;
    public bool refreshFoodList;

    Vector3 leftSpawnVector;

    public GameObject customerPrefab;

    public GameObject[] allCustomers;
    public List<GameObject> customers;
    public List<GameObject> hungryCustomers;

    public GameObject[] foodItemsInScene;
    public List<GameObject> availableNoodlesInScene;
    public List<GameObject> availableBrothInScene;
    public List<GameObject> availableMeatInScene;
    public List<GameObject> availableToppingInScene;

    // Start is called before the first frame update
    void Start()
    {
        leftSpawnVector = new Vector3(leftSpawnLocation.position.x, leftSpawnLocation.position.y, leftSpawnLocation.position.z);
        isOpen = shopLantern.isOn;
        if(timeUntilNextSpawn == 0)
        {
            GenerateSpawnTime();
        }
        UpdateFoodLists();
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfOpen();
        Timer();
        allCustomers = GameObject.FindGameObjectsWithTag("Customer");
        foreach (var item in allCustomers)
        {
            if(!customers.Contains(item))
            {
                customers.Add(item);
                //Debug.Log(item.GetComponent<customerController>().customerSM.CurrentState);
            }
        }        
        numberOfCustomers = customers.Count;
        if (numberOfCustomers < maxNumberOfCustomers && isOpen)
            toSpawnCustomer = true;

        if(toSpawnCustomer)
        {
            SpawnCustomer();
        }

        if(refreshFoodList)
        {
            UpdateFoodLists();
        }
    }

    private void CheckIfOpen()
    {
        isOpen = shopLantern.isOn;
    }

    private void Timer()
    {
        if(numberOfCustomers < maxNumberOfCustomers && isOpen)
        {
            elapsedTime += .01f;
        }
    }

    private void SpawnCustomer()
    {
        if(elapsedTime > timeUntilNextSpawn && isOpen)
        {
            Instantiate(customerPrefab, leftSpawnVector, leftSpawnLocation.rotation);
            toSpawnCustomer = false;
            GenerateSpawnTime();
            elapsedTime = 0;
        }
        
        
    }

    private void GenerateSpawnTime()
    {
        timeUntilNextSpawn = UnityEngine.Random.Range(15f, 45f);
    }

    private void UpdateFoodLists()
    {
        foodItemsInScene = GameObject.FindGameObjectsWithTag("Food Item");

        foreach (var item in foodItemsInScene)
        {
            FoodItem foodItem = item.GetComponent<FoodItem>();
            if (foodItem.itemFoodType.ToString() == "Noodle" && !availableNoodlesInScene.Contains(item.gameObject) && !foodItem.isRequestedForOrder)
            {
                availableNoodlesInScene.Add(item.gameObject);
            }
            else if (foodItem.itemFoodType.ToString() == "Broth" && !availableBrothInScene.Contains(item.gameObject)&& !foodItem.isRequestedForOrder)
            {
                availableBrothInScene.Add(item.gameObject);
            }
            else if (foodItem.itemFoodType.ToString() == "Meat" && !availableMeatInScene.Contains(item.gameObject) && !foodItem.isRequestedForOrder)
            {
                availableMeatInScene.Add(item.gameObject);
            }
            else if (foodItem.itemFoodType.ToString() == "Topping" && !availableToppingInScene.Contains(item.gameObject) && !foodItem.isRequestedForOrder)
            {
                availableToppingInScene.Add(item.gameObject);
            }
        }
        refreshFoodList = false;
    }
}
